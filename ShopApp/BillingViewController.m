//
//  BillingViewController.m
//  ShopApp
//
//  Created by iViprak1 on 10/02/16.
//  Copyright © 2016 viprak. All rights reserved.
//

#import "BillingViewController.h"
#import "Configration.h"
#import "ViewController.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>


@interface BillingViewController ()

@end

@implementation BillingViewController
@synthesize scrollview;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}


-(void)viewWillAppear:(BOOL)animated
{
    scrollview.contentSize=CGSizeMake(320, viewPay.frame.origin.y+viewPay.frame.size.height);
    [self setFormValues];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



















#pragma mark --keyboard delegates
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect scrollFrame = scrollview.frame;
    scrollFrame.size.height = [UIScreen mainScreen].bounds.size.height-65.0-236.0;
    scrollview.frame = scrollFrame;
    vwCalendar.hidden = TRUE;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect scrollFrame = scrollview.frame;
    scrollFrame.size.height = [UIScreen mainScreen].bounds.size.height-65.0;
    scrollview.frame = scrollFrame;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}




















#pragma mark -IBAction

- (IBAction)btnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)btnClickPay:(id)sender
{
    [self setCard];
}


- (IBAction)btnSelectDate:(id)sender
{
    [txtFirstname resignFirstResponder];
    [txtLastname resignFirstResponder];
    [txtCity resignFirstResponder];
    [txtComment resignFirstResponder];
    [txtCountry resignFirstResponder];
    [txtCVV resignFirstResponder];
    [txtState resignFirstResponder];
    [txtzipcode resignFirstResponder];
    [txtAddress1 resignFirstResponder];
    [txtAddress2 resignFirstResponder];

    vwCalendar.hidden = FALSE;
}


- (IBAction)btnCancel:(id)sender
{
    vwCalendar.hidden = TRUE;
}


- (IBAction)btnDone:(id)sender
{
    NSDate *dtExpiry = [dtPicker date];
    NSDateFormatter *frmt = [[NSDateFormatter alloc] init];
    
    [frmt setDateFormat:@"MM"];
    strCardExpiryMonth = [frmt stringFromDate:dtExpiry];
    
    [frmt setDateFormat:@"yy"];
    strCardExpiryYear = [frmt stringFromDate:dtExpiry];
    
    [btnDateDisplay setTitle:[NSString stringWithFormat:@"%@/%@",strCardExpiryMonth,strCardExpiryYear] forState:UIControlStateNormal];
    
    vwCalendar.hidden = TRUE;
}


- (IBAction)btnLogout:(id)sender
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    
    ViewController *controller=[[ViewController alloc]init];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    controller= [storyboard instantiateViewControllerWithIdentifier:@"defaultStoryId"];
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
    navController.navigationBarHidden = YES;
    
    [kpref setBool:false forKey:kSignUp];
    [kpref setBool:true forKey:kSignIn];
    [kpref setBool:NO forKey:kIsuserSignIn];
    [kpref setValue:@"" forKey:kuserEmail];
    
    [UIApplication sharedApplication].keyWindow.rootViewController = navController;
}



















#pragma mark ==USER DEFINE METHODS==
- (void)setFormValues
{
    DatabaseHandler *dataHandler = [[DatabaseHandler alloc] init];
    NSString *strQuery = [NSString stringWithFormat:@"SELECT * FROM BillingInfo WHERE email='%@'",[[NSUserDefaults standardUserDefaults] valueForKey:@"email"]];
    arrBillingDetails = [dataHandler getData:strQuery];
    
    if (arrBillingDetails.count !=0)
    {
        txtFirstname.text = [[arrBillingDetails firstObject] valueForKey:kFirstName];
        txtLastname.text = [[arrBillingDetails firstObject] valueForKey:kLastName];
        txtCardnumber.text = [[arrBillingDetails firstObject] valueForKey:kCardNumber];
        txtAddress1.text = [[arrBillingDetails firstObject] valueForKey:kAddress1];
        txtAddress2.text = [[arrBillingDetails firstObject] valueForKey:kAddress2];
        txtCity.text = [[arrBillingDetails firstObject] valueForKey:kCity];
        txtState.text = [[arrBillingDetails firstObject] valueForKey:kState];
        txtzipcode.text = [[arrBillingDetails firstObject] valueForKey:kZipCode];
        txtCountry.text = [[arrBillingDetails firstObject] valueForKey:kCountry];
        txtComment.text = [[arrBillingDetails firstObject] valueForKey:kComment];
    }
}


- (void)setCard
{
   stripeCard = [[STPCard alloc] init];
    
    stripeCard.name = [NSString stringWithFormat:@"%@ %@",txtFirstname.text, txtLastname.text];
    stripeCard.number = txtCardnumber.text;
    stripeCard.cvc = txtCVV.text;
    stripeCard.expMonth = [strCardExpiryMonth integerValue];
    stripeCard.expYear = [strCardExpiryYear integerValue];
    
    if ([self validateCustomerInfo])
    {
        [self performStripeOperation];
    }
}


- (BOOL)validateCustomerInfo
{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Please try again" message:@"Please enter all required information" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    //Validate card number, CVC, expMonth, expYear
    NSError* error = nil;
    [stripeCard validateCardReturningError:&error];
    
    if (error)
    {
        alert.message = [error localizedDescription];
        [alert show];
        return NO;
    }
    else
    {
        [self saveCard];
    }
    
    return YES;
}


- (void)performStripeOperation
{
    [Stripe createTokenWithCard:stripeCard publishableKey:@"pk_test_2fxXuSuMSDvbMemWqdQZQf82" completion:^(STPToken* token, NSError* error) {
                         if(error)
                             NSLog(@"Error: %@",error);
                        else
                        {
                            [self showSuccessPopup];
                            [self performSelector:@selector(hideSuccessPopup) withObject:nil afterDelay:2.0];
                            NSLog(@"Token: %@",token.tokenId);
                        }
                     }];
}


- (void)saveCard
{
    DatabaseHandler *dataHandler = [[DatabaseHandler alloc] init];
    NSString *strQuery = @"";
    NSString *strMessage = @"";
    
    strQuery = [NSString stringWithFormat:@"SELECT * FROM BillingInfo WHERE emailId=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"email"]];
    NSMutableArray *arrTemp = [dataHandler getData:strQuery];
    
    if ([arrTemp count]==0)
    {
        strQuery = [NSString stringWithFormat:@"INSERT INTO BillingInfo (firstname,lastname,cardnumber,address1,address2,city,state,zipcode,country,comment,email) VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",txtFirstname.text,txtLastname.text,txtCardnumber.text,txtAddress1.text,txtAddress2.text,txtCity.text,txtState.text,txtzipcode.text,txtCountry.text,txtComment.text,[kpref valueForKey:kuserEmail]];
        strMessage = @"Data saved successfully.";
        
        if ([dataHandler dataManipulation:strQuery])
        {
            NSLog(@"Data Save");
        }
    }
}


- (void)showSuccessPopup
{
    vwSuccessPopup.hidden = FALSE;
    vwSuccessPopup.alpha = 1.0;
}


- (void)hideSuccessPopup
{
    [UIView animateWithDuration:1.0 animations:^{
        vwSuccessPopup.alpha = 0.0;
        
    } completion:^(BOOL finished) {
        vwSuccessPopup.hidden = TRUE;
        vwThanks.hidden = FALSE;
    }];
}
@end
