//
//  ViewController.m
//  ShopApp
//
//  Created by iViprak1 on 10/02/16.
//  Copyright © 2016 viprak. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




















#pragma mark -IBAction
- (IBAction)btnSignUp:(id)sender
{
    SignUPINViewController *sc=[[SignUPINViewController alloc]init];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    sc= [storyboard instantiateViewControllerWithIdentifier:@"signUPINview"];
    
    [kpref setBool:true forKey:kSignUp];
    [kpref setBool:false forKey:kSignIn];

    [self.navigationController pushViewController:sc animated:YES];
}


- (IBAction)btnNext:(id)sender
{
    SignUPINViewController *sc=[[SignUPINViewController alloc]init];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    sc= [storyboard instantiateViewControllerWithIdentifier:@"signUPINview"];
    
    [kpref setBool:false forKey:kSignUp];
    [kpref setBool:true forKey:kSignIn];
    [self.navigationController pushViewController:sc animated:YES];
}
@end
