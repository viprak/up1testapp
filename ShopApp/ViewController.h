//
//  ViewController.h
//  ShopApp
//
//  Created by iViprak1 on 10/02/16.
//  Copyright © 2016 viprak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignUPINViewController.h"
#import "Configration.h"

@interface ViewController : UIViewController

- (IBAction)btnSignUp:(id)sender;
- (IBAction)btnNext:(id)sender;

@end

