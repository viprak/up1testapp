//
//  BillingViewController.h
//  ShopApp
//
//  Created by iViprak1 on 10/02/16.
//  Copyright © 2016 viprak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Stripe/Stripe.h>
#import "DatabaseHandler.h"
#import "ProductViewController.h"
@interface BillingViewController : UIViewController<UITextFieldDelegate>
{
    
    IBOutlet UIView *viewPay;
    IBOutlet UITextField *txtFirstname;
    IBOutlet UITextField *txtLastname;
    IBOutlet UITextField *txtCardnumber;
    IBOutlet UITextField *txtAddress1;
    IBOutlet UITextField *txtAddress2;
    IBOutlet UITextField *txtState;
    IBOutlet UITextField *txtCity;
    IBOutlet UITextField *txtzipcode;
    IBOutlet UITextField *txtCountry;
    IBOutlet UITextField *txtComment;
    IBOutlet UITextField *txtCVV;
    IBOutlet UIView *vwCalendar;
    IBOutlet UIView *vwSuccessPopup;
    IBOutlet UIView *vwThanks;
    IBOutlet UIDatePicker *dtPicker;
    IBOutlet UIButton *btnDateDisplay;
    
    STPCard *stripeCard;
    NSString *strCardExpiryMonth;
    NSString *strCardExpiryYear;
    NSMutableArray *arrBillingDetails;
}


@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;
@property (strong, nonatomic) IBOutlet UIButton *btnClickPay;

- (IBAction)btnBack:(id)sender;
- (IBAction)btnClickPay:(id)sender;
- (IBAction)btnCancel:(id)sender;
- (IBAction)btnDone:(id)sender;
- (IBAction)btnSelectDate:(id)sender;
- (IBAction)btnLogout:(id)sender;


- (void)setFormValues;
- (void)setCard;
- (void)saveCard;
- (void)showSuccessPopup;
- (void)hideSuccessPopup;


@end
