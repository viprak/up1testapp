//
//  Configration.h
//  ShopApp
//
//  Created by iViprak1 on 10/02/16.
//  Copyright © 2016 viprak. All rights reserved.
//

#ifndef Configration_h
#define Configration_h


#endif /* Configration_h */


#define kpref [NSUserDefaults standardUserDefaults]

#define kSignUp    @"signup"
#define kSignIn    @"signin"


#pragma mark ==Table Fields==
#define kFirstName @"firstname"
#define kLastName @"lastname"
#define kCardNumber @"cardnumber"
#define kAddress1 @"address1"
#define kAddress2 @"address2"
#define kCity @"city"
#define kState @"state"
#define kZipCode @"zipcode"
#define kCountry @"country"
#define kComment @"comment"

#define kuserEmail @"email"
#define kIsuserSignIn @"userSignIn"