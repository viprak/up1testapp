//
//  SignUPINViewController.m
//  ShopApp
//
//  Created by iViprak1 on 10/02/16.
//  Copyright © 2016 viprak. All rights reserved.
//

#import "SignUPINViewController.h"

@interface SignUPINViewController ()

@end

@implementation SignUPINViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}


-(void)viewWillAppear:(BOOL)animated
{
    if ([kpref boolForKey:kSignUp]==true)
    {
        viewSignIn.hidden=true;
        viewSignUp.hidden=false;
        lblTitle.text=@"Sign up";
        [btnSocial setImage:[UIImage imageNamed:@"btnFacebook.png"] forState:UIControlStateNormal];
    }
    
    if ([kpref boolForKey:kSignIn]==true)
    {
        viewSignIn.hidden=false;
        viewSignUp.hidden=true;
        lblTitle.text=@"Sign in";
        [btnSocial setImage:[UIImage imageNamed:@"btnSignInFacebook.png"] forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



















#pragma mark - IBaction

- (IBAction)btnBack:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (IBAction)btnSignUp:(id)sender
{
    ProductViewController *pvc=[[ProductViewController alloc]init];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    pvc= [storyboard instantiateViewControllerWithIdentifier:@"productview"];
    [self.navigationController pushViewController:pvc animated:YES];
}


- (IBAction)btnFBLogin:(id)sender
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:  @[@"public_profile", @"email", @"user_friends"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error)
         {
             NSLog(@"Process error");
         }
         else if (result.isCancelled)
         {
             NSLog(@"Cancelled");
         }
         else
         {
             NSLog(@"Logged in");
             [self fetchUserInfo];
             ProductViewController *pvc=[[ProductViewController alloc]init];
             UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
             pvc= [storyboard instantiateViewControllerWithIdentifier:@"productview"];
             [self.navigationController pushViewController:pvc animated:YES];
         }
     }];
}


-(void)fetchUserInfo
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        NSLog(@"Token is available : %@",[[FBSDKAccessToken currentAccessToken]tokenString]);
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(large), email, birthday, bio ,location ,friends ,hometown , friendlists"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                 if (!error)
                 {
                     NSString *userEmail=result[@"email"];
                     [kpref setValue:userEmail forKey:kuserEmail];
                     [kpref setBool:YES forKey:kIsuserSignIn];

                 }
                 else
                 {
                     NSLog(@"Error %@",error);
                     
                 }
         }];
    }
}

@end
