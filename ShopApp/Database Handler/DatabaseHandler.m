//
//  DatabaseHandler.m
//  TaskTracker
//
//  Created by Viprak1 on 29/11/13.
//  Copyright (c) 2013 Vipul Patel. All rights reserved.
//

#import "DatabaseHandler.h"

@implementation DatabaseHandler

-(id)init
{
    if (self==[super init])
    {
        [self copyDatabaseToShareFolder];
    }
    return self;
}


-(void)copyDatabase
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *insPath = [NSString stringWithFormat:@"shopping.sqlite"];
    destPath = [documentsDirectory stringByAppendingPathComponent:insPath];
    NSString *srcPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:insPath];
    NSLog(@"\n src %@ \n dest %@", srcPath, destPath);
    if (![[NSFileManager defaultManager] fileExistsAtPath:destPath])
    {
        NSError *error;
        NSLog(@"not exist");
        [[NSFileManager defaultManager] copyItemAtPath:srcPath toPath:destPath error:&error];
    }
    else
    {
        NSLog(@"exist");
    }
}

- (void)copyDatabaseToShareFolder
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths firstObject];
    NSString *insPath = [NSString stringWithFormat:@"shopping.sqlite"];
    destPath = [documentsDirectory stringByAppendingPathComponent:insPath];
    NSString *srcPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:insPath];
    NSLog(@"\n src %@ \n dest %@", srcPath, destPath);
    if (![[NSFileManager defaultManager] fileExistsAtPath:destPath])
    {
        NSError *error;
        NSLog(@"Database not exist");
        [[NSFileManager defaultManager] copyItemAtPath:srcPath toPath:destPath error:&error];
    }
    else
    {
        NSLog(@"Database exist");
    }
}


-(BOOL)dataManipulation: (NSString *)query
{
    BOOL result=NO;
    if (sqlite3_open([destPath UTF8String], &connectDatabase)==SQLITE_OK)
    {
        sqlite3_stmt *stmt;
        
        if (sqlite3_prepare_v2(connectDatabase, [query UTF8String], -1, &stmt, NULL)==SQLITE_OK)
        {
            sqlite3_step(stmt);
            result=YES;
        }
        sqlite3_finalize(stmt);
    }
    
    sqlite3_close(connectDatabase);
    return result;
}


-(NSMutableArray *)getData: (NSString *)query
{
    if ([query isEqualToString:@"SELECT Trans_Id FROM Transaction_Table"])
    {
        NSLog(@"query");
    }
    
    NSMutableArray *arrData=[[NSMutableArray alloc]init];
    if (sqlite3_open([destPath UTF8String],&connectDatabase)==SQLITE_OK)
    {
        sqlite3_stmt *stmt;
        const char *query_stmt = [query UTF8String];
        
        if (sqlite3_prepare_v2(connectDatabase,query_stmt, -1, &stmt, NULL)==SQLITE_OK)
        {
            while (sqlite3_step(stmt)==SQLITE_ROW)
            {
                NSMutableDictionary *dictResult=[[NSMutableDictionary alloc] init];
                
                for (int i=0;i<sqlite3_column_count(stmt);i++)
                {
                    NSString *str;
                
                    if (sqlite3_column_text(stmt,i)!=NULL)
                    {
                        str = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt,i)];
                    }
                    else
                    {
                        str=@"";
                    }
            
                   // NSLog(@"%@",[MyMacros CheckForEmpty:str]?@"no":@"yes");
                    [dictResult setValue:str forKey:[NSString stringWithUTF8String:(char *)sqlite3_column_name(stmt,i)]];
                }
                              
                [arrData addObject:dictResult];
            }
            sqlite3_finalize(stmt);
        }
        sqlite3_close(connectDatabase);
    }
    return arrData;
}


+(NSString *) getDatabasePath:(NSString *)dbName
{
    // Get the path to the documents directory and append the databaseName
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [documentPaths objectAtIndex:0];
    return [documentsDir stringByAppendingPathComponent:dbName];
}
@end
