//
//  DatabaseHandler.h
//  TaskTracker
//
//  Created by Viprak1 on 29/11/13.
//  Copyright (c) 2013 Vipul Patel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DatabaseHandler : NSObject
{
    sqlite3 *connectDatabase;
    NSString *destPath;
}

-(void)copyDatabase;
- (void)copyDatabaseToShareFolder;
-(BOOL)dataManipulation: (NSString *)query;
-(NSMutableArray *)getData: (NSString *)query;

+(NSString *) getDatabasePath:(NSString *)dbName;

@end
