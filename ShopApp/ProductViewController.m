//
//  ProductViewController.m
//  ShopApp
//
//  Created by iViprak1 on 10/02/16.
//  Copyright © 2016 viprak. All rights reserved.
//

#import "ProductViewController.h"
#import "SignUPINViewController.h"
#import "ViewController.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface ProductViewController ()

@end

@implementation ProductViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    arryProduct=[[NSMutableArray alloc] initWithObjects:@"img1.jpg",@"img2.jpg",@"img3.jpg",@"img4.jpg",@"img5.jpg",@"img6.jpg",@"img7.jpg",@"img8.jpg",@"img9.jpg",@"img10.jpg", nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




















#pragma mark -Collectionview
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arryProduct.count;
}


-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CustomCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"custom" forIndexPath:indexPath];
    
    cell.imgProduct.contentMode = UIViewContentModeScaleAspectFill;
    cell.imgProduct.image=[UIImage imageNamed:[arryProduct objectAtIndex:indexPath.row]];
    
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    BillingViewController *bvc=[[BillingViewController alloc]init];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    bvc= [storyboard instantiateViewControllerWithIdentifier:@"billingview"];
    [self.navigationController pushViewController:bvc animated:YES];
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    double dimVal = ([UIScreen mainScreen].bounds.size.width-44)/2;
    return CGSizeMake(dimVal,dimVal);
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(15, 14, 15, 14);
}


@end
