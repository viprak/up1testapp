//
//  ProductViewController.h
//  ShopApp
//
//  Created by iViprak1 on 10/02/16.
//  Copyright © 2016 viprak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomCell.h"
#import "BillingViewController.h"


@interface ProductViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>
{
    NSMutableArray *arryProduct;
}

@property(nonatomic,retain)IBOutlet UICollectionView *collview;

@end
