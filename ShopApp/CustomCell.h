//
//  CustomCell.h
//  ShopApp
//
//  Created by iViprak1 on 10/02/16.
//  Copyright © 2016 viprak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgProduct;

@end
