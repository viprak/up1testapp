//
//  SignUPINViewController.h
//  ShopApp
//
//  Created by iViprak1 on 10/02/16.
//  Copyright © 2016 viprak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "Configration.h"
#import "ProductViewController.h"


@interface SignUPINViewController : UIViewController
{
    IBOutlet UILabel *lblTitle;
    IBOutlet UIView *viewSignIn;
    IBOutlet UIView *viewSignUp;
    IBOutlet UIButton *btnSocial;
}


- (IBAction)btnBack:(id)sender;
- (IBAction)btnSignUp:(id)sender;
- (IBAction)btnFBLogin:(id)sender;
@end
